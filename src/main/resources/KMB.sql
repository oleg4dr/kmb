/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 120000
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : KMB

 Target Server Type    : PostgreSQL
 Target Server Version : 120000
 File Encoding         : 65001

 Date: 05/02/2020 12:22:12
*/


-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS "KMB"."company";
CREATE TABLE "KMB"."company" (
  "id" uuid NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "parent_id" uuid
)
;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO "KMB"."company" VALUES ('14103523-25e4-4d53-867d-1dabf8be99ad', 'LG', NULL);
INSERT INTO "KMB"."company" VALUES ('4369d3d8-5623-4d60-918c-bd976d765a74', 'AMD', NULL);
INSERT INTO "KMB"."company" VALUES ('cf8ccb78-6aa8-4bde-a56e-28c3cc24cf5d', 'Fusion', 'd78815c5-4e62-46a2-8c7e-f6e23844be99');
INSERT INTO "KMB"."company" VALUES ('f8bca7e8-cc3b-42fe-9dcf-cc397384f7bf', 'DAWNGATE', 'd78815c5-4e62-46a2-8c7e-f6e23844be99');
INSERT INTO "KMB"."company" VALUES ('e57d593e-3483-4fbf-9947-1c213cc7d045', 'SUNRISE', 'd78815c5-4e62-46a2-8c7e-f6e23844be99');
INSERT INTO "KMB"."company" VALUES ('a0ae6695-9de9-494f-8023-b2ce246da9af', 'Nokia', '14103523-25e4-4d53-867d-1dabf8be99ad');
INSERT INTO "KMB"."company" VALUES ('98ada00b-2a9a-4e7f-ac11-89130a615114', 'samsamichnichkich', NULL);
INSERT INTO "KMB"."company" VALUES ('d78815c5-4e62-46a2-8c7e-f6e23844be99', 'DELL', NULL);
INSERT INTO "KMB"."company" VALUES ('b8f5bde5-39cd-433c-851a-2bdaf81acea1', 'Intel', '14103523-25e4-4d53-867d-1dabf8be99ad');
INSERT INTO "KMB"."company" VALUES ('822962e1-a751-49dd-9bfd-c673d0a6602f', 'Intelli', '14103523-25e4-4d53-867d-1dabf8be99ad');
INSERT INTO "KMB"."company" VALUES ('37e5ec5a-8459-472f-9441-0923760f97b3', 'Bintela', 'b8f5bde5-39cd-433c-851a-2bdaf81acea1');
INSERT INTO "KMB"."company" VALUES ('1c5ff06b-6d71-4035-b10c-43f548f16c0f', 'just intel sw', 'b8f5bde5-39cd-433c-851a-2bdaf81acea1');
INSERT INTO "KMB"."company" VALUES ('95015250-9c3e-43c3-8009-269ffe528e93', 'iIntellili', 'b8f5bde5-39cd-433c-851a-2bdaf81acea1');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS "KMB"."employee";
CREATE TABLE "KMB"."employee" (
  "id" uuid NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "company_id" uuid NOT NULL,
  "manager_id" uuid
)
;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO "KMB"."employee" VALUES ('2be2d1bc-ca54-446c-94a2-d8d264f394c5', 'Black Bober', '98ada00b-2a9a-4e7f-ac11-89130a615114', '6030d51a-b320-4245-9a83-eb679cec8389');
INSERT INTO "KMB"."employee" VALUES ('6030d51a-b320-4245-9a83-eb679cec8389', 'Bober', '98ada00b-2a9a-4e7f-ac11-89130a615114', 'b46896ff-2ed8-4892-bbef-63c41a251174');
INSERT INTO "KMB"."employee" VALUES ('ffdc9ead-1b85-402f-a98f-bbe48ad923ee', 'Petrovich', '98ada00b-2a9a-4e7f-ac11-89130a615114', NULL);
INSERT INTO "KMB"."employee" VALUES ('b46896ff-2ed8-4892-bbef-63c41a251174', 'Bobini', '98ada00b-2a9a-4e7f-ac11-89130a615114', 'ffdc9ead-1b85-402f-a98f-bbe48ad923ee');
INSERT INTO "KMB"."employee" VALUES ('fb39c3d8-bf80-4fc7-815a-370e7ea695cb', 'r1', '822962e1-a751-49dd-9bfd-c673d0a6602f', NULL);
INSERT INTO "KMB"."employee" VALUES ('6ea57c75-a5c4-42fb-b542-97de38986ea1', 'r2', '822962e1-a751-49dd-9bfd-c673d0a6602f', NULL);
INSERT INTO "KMB"."employee" VALUES ('5f186ef6-1bfe-46a6-aec8-99a1a484f8e7', 'bbb', '95015250-9c3e-43c3-8009-269ffe528e93', NULL);
INSERT INTO "KMB"."employee" VALUES ('6fd742ca-8c23-46ee-9223-318c3af091bc', 'aaa', '95015250-9c3e-43c3-8009-269ffe528e93', NULL);
INSERT INTO "KMB"."employee" VALUES ('b7b09ea0-0c96-4007-b64e-87b9db969d14', 'fadf', '95015250-9c3e-43c3-8009-269ffe528e93', NULL);
INSERT INTO "KMB"."employee" VALUES ('42a1bae5-3645-4e5b-be9a-7d8ec7e3b5c1', 'Si Hoy Chen', '14103523-25e4-4d53-867d-1dabf8be99ad', NULL);
INSERT INTO "KMB"."employee" VALUES ('96fe8668-217f-4a61-9b12-8128f4b0238b', 'Li Shung Pei', '14103523-25e4-4d53-867d-1dabf8be99ad', '42a1bae5-3645-4e5b-be9a-7d8ec7e3b5c1');
INSERT INTO "KMB"."employee" VALUES ('f5d2dc49-a177-4e1e-9b24-1bc8964a2b19', 'Ko Chun Hei', '14103523-25e4-4d53-867d-1dabf8be99ad', NULL);

-- ----------------------------
-- Primary Key structure for table company
-- ----------------------------
ALTER TABLE "KMB"."company" ADD CONSTRAINT "company_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table employee
-- ----------------------------
ALTER TABLE "KMB"."employee" ADD CONSTRAINT "employee_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table employee
-- ----------------------------
ALTER TABLE "KMB"."employee" ADD CONSTRAINT "FK_company_employee" FOREIGN KEY ("company_id") REFERENCES "KMB"."company" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
