/*
 * This file is generated by jOOQ.
 */
package database;


import database.tables.Company;
import database.tables.Employee;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in KMB
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>KMB.company</code>.
     */
    public static final Company COMPANY = Company.COMPANY;

    /**
     * The table <code>KMB.employee</code>.
     */
    public static final Employee EMPLOYEE = Employee.EMPLOYEE;
}
