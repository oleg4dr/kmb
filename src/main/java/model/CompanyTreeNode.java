package model;

import java.util.List;

public class CompanyTreeNode {
    private CompanyDTO parentCompany;
    private List<CompanyDTO> childCompanies;

    public CompanyTreeNode() {
    }

    public void setParentCompany(CompanyDTO parentCompany) {
        this.parentCompany = parentCompany;
    }

    public void setChildCompanies(List<CompanyDTO> childCompanies) {
        this.childCompanies = childCompanies;
    }

    @Override
    public String toString() {
        return "CompanyTreeNode{" +
                "parentCompany=" + parentCompany +
                ", childCompanies=" + childCompanies +
                '}';
    }

    public CompanyDTO getParentCompany() {
        return parentCompany;
    }

    public List<CompanyDTO> getChildCompanies() {
        return childCompanies;
    }
}
