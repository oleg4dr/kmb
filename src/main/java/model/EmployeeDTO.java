package model;

import java.util.UUID;

public class EmployeeDTO {
    private UUID id;
    private String name;
    private UUID company_id;
    private UUID manager_id;

    public EmployeeDTO() {
    }

    public EmployeeDTO(String name, UUID company_id, UUID manager_id) {
        this.name = name;
        this.company_id = company_id;
        this.manager_id = manager_id;
    }

    public EmployeeDTO(UUID id, String name, UUID company_id, UUID manager_id) {
        this.id = id;
        this.name = name;
        this.company_id = company_id;
        this.manager_id = manager_id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getCompany_id() {
        return company_id;
    }

    public void setCompany_id(UUID company_id) {
        this.company_id = company_id;
    }

    public UUID getManager_id() {
        return manager_id;
    }

    public void setManager_id(UUID manager_id) {
        this.manager_id = manager_id;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", company_id=" + company_id +
                ", manager_id=" + manager_id +
                '}';
    }
}
