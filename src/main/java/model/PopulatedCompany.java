package model;

public class PopulatedCompany {
    private CompanyDTO company;
    private int employees;

    public PopulatedCompany(CompanyDTO company, int employees) {
        this.company = company;
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "PopulatedCompany{" +
                "company=" + company +
                ", employees=" + employees +
                '}';
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }
}
