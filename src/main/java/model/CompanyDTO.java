package model;

import java.util.UUID;

public class CompanyDTO {

    private UUID id;
    private String name;
    private UUID parent_id;

    public CompanyDTO(UUID id, UUID parent_id, String name) {
        this.id = id;
        this.parent_id = parent_id;
        this.name = name;
    }



    public CompanyDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getParent_id() {
        return parent_id;
    }

    public void setParent_id(UUID parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CompanyDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parent_id=" + parent_id +
                '}';
    }
}
