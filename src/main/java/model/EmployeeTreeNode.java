package model;

import java.util.List;

public class EmployeeTreeNode {
    private EmployeeDTO parentEmployee;
    private List<EmployeeDTO> childEmployees;

    @Override
    public String toString() {
        return "EmployeeTreeNode{" +
                "parentEmployee=" + parentEmployee +
                ", childEmployees=" + childEmployees +
                '}';
    }

    public EmployeeTreeNode() {
    }

    public EmployeeTreeNode(EmployeeDTO parentEmployee, List<EmployeeDTO> childEmployees) {
        this.parentEmployee = parentEmployee;
        this.childEmployees = childEmployees;
    }

    public EmployeeDTO getParentEmployee() {
        return parentEmployee;
    }

    public void setParentEmployee(EmployeeDTO parentEmployee) {
        this.parentEmployee = parentEmployee;
    }

    public List<EmployeeDTO> getChildEmployees() {
        return childEmployees;
    }

    public void setChildEmployees(List<EmployeeDTO> childEmployees) {
        this.childEmployees = childEmployees;
    }
}
