package model;

public class PopulatedEmployee {
    private EmployeeDTO employee;
    private String managerName;
    private String companyName;

    @Override
    public String toString() {
        return "PopulatedEmployee{" +
                "employee=" + employee +
                ", managerName='" + managerName + '\'' +
                ", companyName='" + companyName + '\'' +
                '}';
    }

    public PopulatedEmployee() {
    }

    public PopulatedEmployee(EmployeeDTO employee, String managerName, String companyName) {
        this.employee = employee;
        this.managerName = managerName;
        this.companyName = companyName;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
