package kmb.back;


import model.CompanyDTO;
import model.EmployeeDTO;
import model.PopulatedCompany;
import model.PopulatedEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class Controller {

    final
    ServiceImplementation serviceImplementation;

    public Controller(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }

    @Autowired
    private Environment environment;

    //Вывод списка всех компаний/сотрудников

    @GetMapping(value = "org/companies")
     List<CompanyDTO> getCompanies(){
        return serviceImplementation.getComps();
    }

    @GetMapping(value = "emp/employees")
    List<EmployeeDTO> getEmployees(){
        return serviceImplementation.getEmployees();
    }

    //Вывод одного конкретного компании/сотрудника

    @GetMapping("/org/company/{id}")
    ResponseEntity<?> getCompany(@PathVariable UUID id) {
        Optional<CompanyDTO> company =serviceImplementation.getCompanie(id);
        return company.map(response ->  ResponseEntity.ok().body(response)).
                orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/emp/employee/{id}")
    ResponseEntity<?> getEmployee(@PathVariable UUID id) {
        Optional<EmployeeDTO> employee =serviceImplementation.getCEmployee(id);
        return employee.map(response ->  ResponseEntity.ok().body(response)).
                orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    //Создание компании/сотрудника

    @PostMapping(value = "org/company")
    ResponseEntity<CompanyDTO> createCompany(@RequestBody CompanyDTO company) throws URISyntaxException{
        System.out.println("Request to create company: " + company.toString());
        CompanyDTO res = serviceImplementation.addComp(company);
        if (res!=null)
            return ResponseEntity.created(new URI("/api/company/" + res.getId())).body(res);
        else {
            System.out.println("Error: id of parent company is invalid");
            return new ResponseEntity("Error id of parent company is invalid", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "emp/employee")
    ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO employee) throws URISyntaxException{
        System.out.println("Request to create employee: " + employee.toString());
        EmployeeDTO res = serviceImplementation.addEmployee(employee);
        //TODO - потеряли валидацию менеджера
//        if (res!= null)
//            return new ResponseEntity<>(res, HttpStatus.OK);
//        else
//            return new ResponseEntity<>("manager is not in the same company", HttpStatus.BAD_REQUEST);
        return ResponseEntity.created(new URI("/api/employee/" + res.getId()))
                .body(res);
    }

    //Изменение компании/сотрудника

    @PutMapping("/org/company")
    ResponseEntity<CompanyDTO> updateCompany(@RequestBody CompanyDTO company){
        System.out.println("Request to update company: " + company.toString());
        CompanyDTO res = serviceImplementation.editComp(company);
        if(res!=null)
        {
            System.out.println("Request succeeded");
            return ResponseEntity.ok().body(res);
        }
        else{
            System.out.println("Request failed");
            return new ResponseEntity("This parent company doesn't exist", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/emp/employee")
    ResponseEntity<EmployeeDTO> updateEmployee(@RequestBody EmployeeDTO employee){
        System.out.println("Request to update employee: " + employee.toString());
        EmployeeDTO res = serviceImplementation.editEmp(employee);
//        if (res!= null)
//            return new ResponseEntity<>(res, HttpStatus.OK);
//        else
//            return new ResponseEntity<>("manager is not in the same company", HttpStatus.BAD_REQUEST);
        return ResponseEntity.ok().body(res);
    }


    //Удаление компании/работника

    @DeleteMapping("/org/del/{id}")
    public ResponseEntity<?> deleteCompany(@PathVariable UUID id){
        System.out.println("Request to delete company: " + id);
        ResponseEntity res = serviceImplementation.deleteCompanyById(id);
        if(res.getStatusCode().equals(HttpStatus.OK))
            System.out.println("Company deleted successfully");
        else
            System.out.println("Error while deleting company");
        return res;
    }

    @DeleteMapping("/emp/del/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable UUID id){
        System.out.println("Request to delete employee: " + id);
        ResponseEntity res = serviceImplementation.deleteEmployeeById(id);
        if(res.getStatusCode().equals(HttpStatus.OK))
            System.out.println("Employee deleted successfully");
        else
            System.out.println("Error while deleting employee");
        return res;
    }


    //поиск компаний
    @GetMapping("/org/search")
    public ResponseEntity<?> searchCompany
    (@RequestParam(required = false) Integer maxRecords, @RequestParam  String name, @RequestParam(required = false) Integer pageNum){
        if(maxRecords==null || maxRecords>Integer.parseInt(environment.getProperty("result.size.maximum")))
        {
            System.out.println("Requested parameter maxRecords was too big or empty, reassigning it with default value");
            maxRecords=Integer.parseInt(environment.getProperty("result.size.default"));
        }
        if(pageNum==null)
            pageNum=0;
        System.out.println("Request search for companies with name: " + name + " with max number of results: " + maxRecords);
        List<PopulatedCompany> res = serviceImplementation.searchCompanies(name, maxRecords, pageNum);
        return ResponseEntity.ok().body(res);
    }

    //поиск сотрудников
    @GetMapping("/emp/search")
    public ResponseEntity<?> searchEmployee
    (@RequestParam(required = false) Integer maxRecords, @RequestParam(required = false) String employeeName,
     @RequestParam(required = false) String companyName, @RequestParam(required = false) Integer pageNum)
    {
        if(maxRecords==null || maxRecords>Integer.parseInt(environment.getProperty("result.size.maximum")))
        {
            System.out.println("Requested parameter maxRecords was too big or empty, reassigning it with default value");
            maxRecords=Integer.parseInt(environment.getProperty("result.size.default"));
        }
        if(pageNum==null)
            pageNum=0;
        System.out.println("Request search for employees with name: " + employeeName + " company: " + companyName + " with max number of results: " + maxRecords);
        List<PopulatedEmployee> res = serviceImplementation.searchEmployes(employeeName, companyName, maxRecords, pageNum);
        return ResponseEntity.ok().body(res);
    }

    //компании в древовидной форме от целевого
    @GetMapping("/org/treenode")
    public ResponseEntity<?> companyTreeNode(@RequestBody CompanyDTO company){
        return ResponseEntity.ok().body(serviceImplementation.getCompanyTreeNode(company));
    }

    //компании в древовидной форме от целевого
    @GetMapping("/emp/treenode")
    public ResponseEntity<?> employeeTreeNode(@RequestBody EmployeeDTO employee){
        return ResponseEntity.ok().body(serviceImplementation.getEmployeeTreeNode(employee));
    }


}
