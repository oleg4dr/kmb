package kmb.back;

import database.tables.Company;
import model.*;
import org.jooq.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static database.Tables.COMPANY;
import static database.Tables.EMPLOYEE;
import static org.jooq.impl.DSL.count;

@Service
public class ServiceImplementation {

    @Autowired
    private DSLContext create;

    //проверка что менеджер из этой же организации
    private boolean checkSameOrg(EmployeeDTO employee){
        EmployeeDTO manager = create.select().from(EMPLOYEE)
                .where(EMPLOYEE.ID.eq(employee.getManager_id()))
                .fetchOne().into(EmployeeDTO.class);
        if(employee.getCompany_id().equals(manager.getCompany_id()))
            return true;
        else return false;
    }

    //API получение списка организаций
    public List<CompanyDTO> getComps(){
        return create.select().from(COMPANY).fetch().into(CompanyDTO.class);
    }

    //API получение списка сотрудиков
    public List<EmployeeDTO> getEmployees(){
        return create.select().from(EMPLOYEE).fetch().into(EmployeeDTO.class);
    }

    //API получить организацию если есть
    public Optional<CompanyDTO> getCompanie(UUID id){
        CompanyDTO res = create.select().from(COMPANY).where(COMPANY.ID.eq(id)).fetchOne().into(CompanyDTO.class);
        if(res.equals(null))
            return null;
        else return Optional.of(res);
    }

    //API получить сотрудника если есть
    public Optional<EmployeeDTO> getCEmployee(UUID id){
        EmployeeDTO res = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(id)).fetchOne().into(EmployeeDTO.class);
        if(res.equals(null))
            return null;
        else return Optional.of(res);
    }


    //API создание организации
    //TODO - подумай еще, автогенерация UUID точно корявое решение
    @Transactional
    public CompanyDTO addComp(CompanyDTO company){
        UUID newCompanyId = UUID.randomUUID();
        //если нет родителей или родитель существует и корректен проведи транзакцию
        if (company.getParent_id().equals(null) || create.fetchExists(create.selectFrom(COMPANY).where(COMPANY.ID.eq(company.getParent_id()))))
        {
            create.insertInto(COMPANY)
                    .values(newCompanyId, company.getName(), company.getParent_id())
                    .execute();

            CompanyDTO res = create.select().from(COMPANY).where(COMPANY.ID.eq(newCompanyId)).fetchOne().into(CompanyDTO.class);
            return res;
        }
        else return null;
    }

    //API создание сотрудника (при выборе организации можно выбрать руководителя только из этой организации)
    //пока будет проверка на одну и ту же организацию
    @Transactional
    public EmployeeDTO addEmployee(EmployeeDTO employee){
        UUID newEmployeeId = UUID.randomUUID();
        if (employee.getManager_id()!=null)
        {
            if(checkSameOrg(employee)){
                create.insertInto(EMPLOYEE)
                        .values(newEmployeeId, employee.getName(), employee.getCompany_id(), employee.getManager_id())
                        .execute();

                EmployeeDTO res = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(newEmployeeId)).fetchOne().into(EmployeeDTO.class);
                return res;
            }
            else return null;
        }
        else
        {
            create.insertInto(EMPLOYEE)
                    .values(newEmployeeId, employee.getName(), employee.getCompany_id(), employee.getManager_id())
                    .execute();

            EmployeeDTO res = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(newEmployeeId)).fetchOne().into(EmployeeDTO.class);
            return res;
        }
    }


    //API изменение организации
    @Transactional
    public CompanyDTO editComp(CompanyDTO company){
        if (company.getParent_id().equals(null) || create.fetchExists(create.selectFrom(COMPANY).where(COMPANY.ID.eq(company.getParent_id()))))
        {
            create.update(COMPANY)
                    .set(COMPANY.NAME, company.getName())
                    .set(COMPANY.PARENT_ID, company.getParent_id())
                    .where(COMPANY.ID.eq(company.getId()))
                    .execute();
            CompanyDTO res = create.select().from(COMPANY).where(COMPANY.ID.eq(company.getId())).fetchOne().into(CompanyDTO.class);
            return res;
        }
        else return null;
    }

    //API изменение сотрудника (при выборе организации можно выбрать руководителя только из этой организации)
    @Transactional
    public EmployeeDTO editEmp(EmployeeDTO employee){
        if (employee.getManager_id()!=null)
        {
            if(checkSameOrg(employee)){
                return getEmployeeDTO(employee);
            }
            else return null;
        }
        else
        {
            return getEmployeeDTO(employee);
        }
    }
    //автоген от дупликатов
    private EmployeeDTO getEmployeeDTO(EmployeeDTO employee) {
        create.update(EMPLOYEE)
                .set(EMPLOYEE.ID, employee.getId())
                .set(EMPLOYEE.COMPANY_ID, employee.getCompany_id())
                .set(EMPLOYEE.MANAGER_ID, employee.getManager_id())
                .set(EMPLOYEE.NAME, employee.getName())
                .where(EMPLOYEE.ID.eq(employee.getId()))
                .execute();
        EmployeeDTO res = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(employee.getId())).fetchOne().into(EmployeeDTO.class);
        return res;
    }


    //API удаление организации: если у организации есть дочерние элементы, удалить ее нельзя
    @Transactional
    public ResponseEntity deleteCompanyById(UUID id){
        CompanyDTO company = create.select().from(COMPANY).where(COMPANY.ID.eq(id)).fetchOne().into(CompanyDTO.class);
        if(create.fetchExists(create.selectFrom(COMPANY).where(COMPANY.PARENT_ID.eq(id))))
        {
            return new ResponseEntity("ERROR: employee " + company.getName() + " is a parent", HttpStatus.BAD_REQUEST);
        }
        else
        {
            create.delete(COMPANY).where(COMPANY.ID.eq(id)).execute();
            return new ResponseEntity("Company: " + company.getName() + " deleted", HttpStatus.OK);
        }
    }


    //API удаление сотрудника: если у сотрудника есть дочерние элементы, удалить его нельзя
    @Transactional
    public ResponseEntity deleteEmployeeById(UUID id){
        EmployeeDTO employee = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(id)).fetchOne().into(EmployeeDTO.class);
        if(create.fetchExists(create.selectFrom(EMPLOYEE).where(EMPLOYEE.MANAGER_ID.eq(id))))
        {
            return new ResponseEntity("ERROR: employee " + employee.getName() + " is a manager", HttpStatus.BAD_REQUEST);
        }
        else
        {
            create.delete(EMPLOYEE).where(EMPLOYEE.ID.eq(id)).execute();
            return new ResponseEntity("Employee: " + employee.getName() + " deleted", HttpStatus.OK);
        }
    }

    //API список организаций с количеством работающих в ней сотрудников с параметрами: пейджинг и поиск по наименованию
    @Transactional
    public List<PopulatedCompany> searchCompanies(String name, int maxRecords, int pageNum){
        List<PopulatedCompany> resultSet = new ArrayList<>();
        Result<Record4<UUID, String, UUID, Integer>> res = create.select(COMPANY.ID, COMPANY.NAME, COMPANY.PARENT_ID, count())
                .from(COMPANY, EMPLOYEE)
                .where(COMPANY.ID.eq(EMPLOYEE.COMPANY_ID), COMPANY.NAME.contains(name))
                .groupBy(COMPANY.ID)
                .limit(maxRecords)
                .offset(pageNum) //пагинация делается так
                .fetch();
        for(Record r: res)
        {
            resultSet.add(new PopulatedCompany(new CompanyDTO(r.get(COMPANY.ID), r.get(COMPANY.PARENT_ID), r.get(COMPANY.NAME)), r.get(count())));
        }
        return resultSet;
    }

    //API список сотрудников (в списке показывать еще его организацию и руководителя) с параметрами: пейджинг и поиск по наименованию организации и/или сотрудника
    @Transactional
    public List<PopulatedEmployee> searchEmployes(String employeeName, String companyName, int maxRecords, int pageNum){
        List<PopulatedEmployee> resultSet = new ArrayList<>();
        Result<Record5<UUID, String, UUID, UUID, String>> res = create.select(EMPLOYEE.ID, EMPLOYEE.NAME, EMPLOYEE.COMPANY_ID, EMPLOYEE.MANAGER_ID, COMPANY.NAME)
                .from(COMPANY, EMPLOYEE)
                .where(EMPLOYEE.COMPANY_ID.eq(COMPANY.ID), COMPANY.NAME.contains(companyName), EMPLOYEE.NAME.contains(employeeName))
                .limit(maxRecords)
                .offset(pageNum)
                .fetch();
        for(Record r: res)
        {
            EmployeeDTO manager = create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(r.get(EMPLOYEE.MANAGER_ID))).fetchOne().into(EmployeeDTO.class);
            resultSet.add(new PopulatedEmployee
                    (new EmployeeDTO(r.get(EMPLOYEE.ID), r.get(EMPLOYEE.NAME), r.get(EMPLOYEE.MANAGER_ID), r.get(EMPLOYEE.MANAGER_ID)),
                            manager.getName(), r.get(COMPANY.NAME)));
        }
        return resultSet;
    }



    //API список организаций в древовидной форме с пейджингом на дочерние элементы
    @Transactional
    public CompanyTreeNode getCompanyTreeNode(CompanyDTO company){
        CompanyTreeNode tree = new CompanyTreeNode();
        if(company.getParent_id()!=null)
            tree.setParentCompany(create.select().from(COMPANY).where(COMPANY.ID.eq(company.getParent_id())).fetchOne().into(CompanyDTO.class));
        tree.setChildCompanies(create.select().from(COMPANY).where(COMPANY.PARENT_ID.eq(company.getId())).fetch().into(CompanyDTO.class));
        return tree;
    }

    //API список сотрудников в древовидной форме с пейджингом на дочерние элементы
    @Transactional
    public EmployeeTreeNode getEmployeeTreeNode(EmployeeDTO employee){
        EmployeeTreeNode tree = new EmployeeTreeNode();
        if(employee.getManager_id()!=null)
            tree.setParentEmployee(create.select().from(EMPLOYEE).where(EMPLOYEE.ID.eq(employee.getManager_id())).fetchOne().into(EmployeeDTO.class));
        tree.setChildEmployees(create.select().from(EMPLOYEE).where(EMPLOYEE.MANAGER_ID.eq(employee.getId())).fetch().into(EmployeeDTO.class));
        return tree;
    }
}
