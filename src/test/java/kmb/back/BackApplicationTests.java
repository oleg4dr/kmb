package kmb.back;


import database.tables.Company;
import model.CompanyDTO;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static database.Tables.COMPANY;
import static database.Tables.EMPLOYEE;
import static org.jooq.impl.DSL.count;


@SpringBootTest
class BackApplicationTests {

	@Autowired
	private DSLContext create;

	@Test
	void contextLoads() {
		String name="";
		int maxRecords=5;
		Result<?> res = create.select(COMPANY.ID, COMPANY.NAME, COMPANY.PARENT_ID, count())
				.from(COMPANY, EMPLOYEE)
				.where(COMPANY.ID.eq(EMPLOYEE.ID), COMPANY.NAME.contains(name))
				.groupBy(COMPANY.ID)
				.fetch();
		for(Record r: res)
		{
			CompanyDTO company = new CompanyDTO(r.get(COMPANY.ID), r.get(COMPANY.PARENT_ID), r.get(COMPANY.NAME));
			int count = r.get(count());
			System.out.println("Company: " + company.toString() + " have " + count + " employees");
		}

	}




}
