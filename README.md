**Получение списка всех компаний/сотрудников**

*Тело запроса пустое*

`GET api/org/companies`

`GET api/emp/employees`

**Получить одного компанию/сотрудника по id**

*Тело запроса пустое*

`GET api/org/company/{id}`

`GET api/emp/employee/{id}`

**Добавить компанию/сотрудника**

*Тело запроса должно содержать новый объект типа компания/сотрудник*

`POST api/org/company`

`POST api/emp/employee`

**Изменить компанию/сотрудника**

*Тело запроса должно содержать новый объект типа компания/сотрудник*

`PUT api/org/company`

`PUT api/emp/employee`

**Удалить компанию/сотрудника по id**

*Тело запроса пустое*

`DELETE api/org/del/`

`DELETE api/emp/del/`